Demo Ansible Playbook project using environment provisioned with Vagrant
=========

Requirements
------------

`VirtualBox` and `Vagrant` are needed to be installed on the HOST computer for a fully run of the entire project

To run just the Ansible part, only `Ansible` needs to be installed on the HOST computer



1. Bring up a VirtualBox VM using Ubuntu 16.04 distro for testing purposes

```
vagrant up
```

2. By default the ansible playbook will automatically be run at 'vagrant up'
In order to disable this feature please comment the following code block
from the Vagrantfile:

```
config.vm.provision "ansible_local" do |ansible|
  ansible.playbook = "demo-playbook.yml"
end
```

You can also manually provisioning the Vagrant environment by using the following command:

```
vagrant provision
```

You can delete the Vagrant environment entirely by using the following command:

```
vagrant destroy
```

3. If it's desired to run the playbook separately from Vagrant you can use the following
command:
```
ansible-playbook demo-playbook.yml -i inventory
```

```
Note: you must have Ansible installed on the host computer!
```

Author Information
------------------

`Robert Daniel Jitaru`
