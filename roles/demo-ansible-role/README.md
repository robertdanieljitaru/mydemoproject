`demo-ansiblle-roles`
=========

Ansible role for restarting myservice.service if variable "MY_VARIABLE"
changed in "/tmp/var_output.ansible"


Role Variables
--------------
Default variables are already set in /defaults/main.yml
```
demo_variable: "MY_VARIABLE=DEFAULT"
demo_output_file: "/tmp/default"
demo_service: "puppet"
```

The same variables are set in the main playbook which replace the default ones for a better approach and consistency. (IF a role is used by multiple playbooks any change of role variables can affect the other projects)


Author Information
------------------

`Robert Daniel Jitaru`
